//
//  main.m
//  MiboRibbit
//
//  Created by launch05 on 13-11-9.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MRAppDelegate class]));
    }
}
