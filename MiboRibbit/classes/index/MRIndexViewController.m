//
//  MRIndexViewController.m
//  MiboRibbit
//
//  Created by Vincent on 13-11-9.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRIndexViewController.h"
#import "MRPianoKeyboard.h"
#import "MRRemoteControllerViewController.h"
#import "MRWallPapersViewController.h"
#import "MRAboutViewController.h"

#import "MRBlueTooth.h"

@interface MRIndexViewController ()

@property (nonatomic, assign) IBOutlet UIImageView *rabbitImageView;
@property (nonatomic, assign) IBOutlet UIImageView *signalImageView;
@property (retain, nonatomic) IBOutlet MRPianoKeyboard *pianoKeyboard;
@property (nonatomic, assign) BOOL isKeyboardShown;

@property (nonatomic, assign) AVPlayer *animationPlayer;
@property (nonatomic, assign) AVPlayerLayer *animationPlayerLayer;
@property (nonatomic, assign) AVAudioPlayer *animationAudioPlayer;

@property (nonatomic, assign) BOOL isPlaying;

@end

@implementation MRIndexViewController

- (void)dealloc
{
    [_pianoKeyboard release];
    [_animationPlayer release];
    [_animationPlayerLayer release];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.wantsFullScreenLayout = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pianoKeyboard.backgroundColor = [UIColor clearColor];
    [_pianoKeyboard addSubview:[MRPianoKeyboard pianoKeyboard]];
    
    [self hidePianoKeyboard:NO];
    
        //BT notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onBTConnected:)
                                                 name:kMRBluetoothConnectedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onBtDisconnected:)
                                                 name:kMRBluetoothDisconnectedNotification
                                               object:nil];
    
        //animation player
    _animationAudioPlayer = [[AVAudioPlayer alloc] init];
    
    [self changeWallPaper:0];
    
//        //connect to Mibo Device
//    double delayInSeconds = 2.0;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        [[MRBlueTooth sharedInstance] start];
//        
//        double delayInSeconds = 2.0;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [[MRBlueTooth sharedInstance] scan];
//        });
//    });
    
//    [[MRBlueTooth sharedInstance] start];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - change WallPaper

- (void)changeWallPaper:(NSInteger)paperNo
{
    NSString *mp4 = [NSString stringWithFormat:@"MR_Ear_Video_4_%d", paperNo];
    
    AVAsset *as = [AVAsset assetWithURL:[[NSBundle mainBundle] URLForResource:mp4 withExtension:@"mp4"]];
    AVPlayerItem *pi = [AVPlayerItem playerItemWithAsset:as];
    
    if(_animationPlayer)
    {
        [_animationPlayer replaceCurrentItemWithPlayerItem:pi];
        return;
    }
    
    __block MRIndexViewController *myself = self;
    
    _animationPlayer = [[AVPlayer alloc] initWithPlayerItem:pi];
    
    [_animationPlayer addBoundaryTimeObserverForTimes:@[[NSValue valueWithCMTime:as.duration]] queue:dispatch_get_main_queue() usingBlock:^{
        [myself.animationPlayer seekToTime:kCMTimeZero];
    }];
    
    _animationPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:_animationPlayer];
    _animationPlayerLayer.frame = [UIScreen mainScreen].bounds;
    _animationPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    _rabbitImageView.image = nil;
    [_rabbitImageView.layer addSublayer:_animationPlayerLayer];
    _rabbitImageView.alpha = 0.0;
    
    self.isPlaying = YES;
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         myself.rabbitImageView.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         myself.isPlaying = NO;
                     }];
}

#pragma mark - buttons event

- (IBAction)btnGoWebClick:(id)sender
{
    NSURL *u = [NSURL URLWithString:@"http://www.mibokids.com"];
    [[UIApplication sharedApplication] openURL:u];
}

- (IBAction)btnBTConnectClick:(id)sender
{

}

- (IBAction)btnRemoteControllerClick:(id)sender
{
    MRRemoteControllerViewController *ctrl = [[MRRemoteControllerViewController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ctrl animated:YES completion:nil];
    [ctrl release];
}

- (IBAction)btnWallPapersClick:(id)sender
{
    MRWallPapersViewController *ctrl = [[MRWallPapersViewController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ctrl animated:YES completion:nil];
    [ctrl release];
}

- (IBAction)btnAboutClick:(id)sender
{
    MRAboutViewController *ctrl = [[MRAboutViewController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ctrl animated:YES completion:nil];
    [ctrl release];
}

- (IBAction)btnKeyboardClick:(id)sender
{
    if (_isKeyboardShown)
    {
        [self hidePianoKeyboard:YES];
    }
    else
    {
        [self showPianoKeyboard:YES];
    }
}

#pragma mark - Mibo touch event

- (void)playTouchAnimation:(NSString *)ani
{
    if (_isPlaying)
    {
        return;
    }
    
    __block MRIndexViewController *myself = self;
    __block id observer = nil;
    
    AVAsset *as = [AVAsset assetWithURL:[[NSBundle mainBundle] URLForResource:ani withExtension:@"mp4"]];
    AVPlayerItem *pi = [AVPlayerItem playerItemWithAsset:as];
    [_animationPlayer replaceCurrentItemWithPlayerItem:pi];
    
    
    observer = [_animationPlayer addBoundaryTimeObserverForTimes:@[[NSValue valueWithCMTime:as.duration]] queue:dispatch_get_main_queue() usingBlock:^{
        myself.isPlaying = NO;
        [myself.animationPlayer seekToTime:kCMTimeZero];
        [myself.animationPlayer removeTimeObserver:observer];
    }];
    
    [_animationPlayer play];
    
    _isPlaying = YES;
}

- (IBAction)btnEarTouch:(id)sender
{
    [self playTouchAnimation:@"MR_Ear_Video_4_0"];
}

- (IBAction)btnFaceTouch:(id)sender
{
    [self playTouchAnimation:@"MR_Face_Video_4_0"];
}

#pragma mark - piano keyboard

- (void)showPianoKeyboard:(BOOL)animated
{
    if (_pianoKeyboard.alpha == 1.0)
    {
        return;
    }
    
    CGPoint np = _pianoKeyboard.center;
    np.y -= _pianoKeyboard.frame.size.height;
    
    if (animated)
    {
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             _pianoKeyboard.center = np;
                             _pianoKeyboard.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             _isKeyboardShown = YES;
                         }];
    }
    else
    {
        _pianoKeyboard.center = np;
        _pianoKeyboard.alpha = 1.0;
        _isKeyboardShown = YES;
    }
}

- (void)hidePianoKeyboard:(BOOL)animated
{
    if (_pianoKeyboard.alpha == 0.0)
    {
        return;
    }
    
    CGPoint np = _pianoKeyboard.center;
    np.y += _pianoKeyboard.frame.size.height;
    
    if (animated)
    {
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             _pianoKeyboard.center = np;
                             _pianoKeyboard.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             _isKeyboardShown = NO;
                         }];
    }
    else
    {
        _pianoKeyboard.center = np;
        _pianoKeyboard.alpha = 0.0;
        _isKeyboardShown = NO;
    }
}

#pragma mark - BluetoothManager bluetooth notification

- (void)onBTConnected:(NSNotification *)notify
{
    _signalImageView.image = [UIImage imageNamed:@"signal-blue"];
}

- (void)onBtDisconnected:(NSNotification *)notify
{
    _signalImageView.image = [UIImage imageNamed:@"signal-red"];
}

@end
