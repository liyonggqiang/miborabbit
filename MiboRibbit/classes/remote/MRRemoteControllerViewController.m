//
//  MRRemoteControllerViewController.m
//  MiboRibbit
//
//  Created by Li Steven on 13-11-10.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRRemoteControllerViewController.h"
#import "MRBlueTooth.h"
#import "MRAreaButton.h"

@interface MRRemoteControllerViewController ()

@end

@implementation MRRemoteControllerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (!IS_IPHONE5)
    {
        nibNameOrNil = @"MRRemoteControllerViewController-ip4";
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - buttons event

- (IBAction)btnBackClick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnNumberClick:(UIButton *)sender
{
    MRBlueTooth *bt = [MRBlueTooth sharedInstance];
    
    switch (sender.tag) {
        case 200:
            [bt sendCommand:MRBTCommandNumber0];
            break;
            
        case 201:
            [bt sendCommand:MRBTCommandNumber1];
            break;
            
        case 202:
            [bt sendCommand:MRBTCommandNumber2];
            break;
            
        case 203:
            [bt sendCommand:MRBTCommandNumber3];
            break;
            
        case 204:
            [bt sendCommand:MRBTCommandNumber4];
            break;
            
        case 205:
            [bt sendCommand:MRBTCommandNumber5];
            break;
            
        case 206:
            [bt sendCommand:MRBTCommandNumber6];
            break;
            
        case 207:
            [bt sendCommand:MRBTCommandNumber7];
            break;
            
        case 208:
            [bt sendCommand:MRBTCommandNumber8];
            break;
            
        case 209:
            [bt sendCommand:MRBTCommandNumber9];
            break;
            
        default:
            break;
    }
}

- (IBAction)btnVolumeClick:(MRAreaButton *)sender
{
    if (MRAreaButtonAreaTop == sender.clickedArea)
    {
        NSLog(@"volue +");
    }
    else
    {
        NSLog(@"volue -");
    }
}

- (IBAction)btnPrevNextClick:(MRAreaButton *)sender
{
    if (MRAreaButtonAreaTop == sender.clickedArea)
    {
        NSLog(@"prev");
    }
    else
    {
        NSLog(@"next");
    }
}

- (IBAction)btnButtonClick:(UIButton *)sender
{
    
}

@end
