//
//  MRWallPapersViewController.m
//  MiboRibbit
//
//  Created by Li Steven on 13-12-10.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRWallPapersViewController.h"

@interface MRWallPapersViewController ()

@end

@implementation MRWallPapersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (IBAction)btnBackClick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
