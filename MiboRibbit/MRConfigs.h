//
//  MRConfigs.h
//  MiboRibbit
//
//  Created by launch05 on 13-11-20.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#define IS_IPHONE5 (568.0f==[UIScreen mainScreen].bounds.size.height)
#define IS_IOS7  (__IPHONE_OS_VERSION_MAX_ALLOWED >= 70000) && ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)


#pragma mark -

#define VT_NOTIFY_INFO_KEY @"VT_NOTIFY_INFO_KEY"
#define VT_POST_NOTIFICATION(a,b) ([[NSNotificationCenter defaultCenter] postNotificationName:a object:nil userInfo:@{VT_NOTIFY_INFO_KEY:b}])