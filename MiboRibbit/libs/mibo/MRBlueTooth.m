//
//  MRBlueTooth.m
//  MiboRibbit
//
//  Created by launch05 on 13-11-11.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRBlueTooth.h"


static const NSInteger TIMEOUT_LIMIT = 10;
uint16_t bt_localid;
char bt_localname[10] = {'P','I','C','B','T','0','0','0','0',0x00};

@interface MRBlueTooth ()

@property (nonatomic, retain) id currentDevice;
@property (nonatomic, assign) NSMutableArray *devices;
@property (nonatomic, assign) NSInteger timeCounter;

@end

@implementation MRBlueTooth

+ (MRBlueTooth *)sharedInstance
{
    static MRBlueTooth *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[MRBlueTooth alloc] init];
    });
    
    return _instance;
}

#pragma mark -

- (id)init
{
    self = [super init];
    if (self)
    {
        _devices = [[NSMutableArray alloc] init];
        [self setup];
    }
    return self;
}

- (void)setup
{
    [self btstackSetupManager];
}

- (void)start
{
    [self btstackStart];
}

- (void)scan
{
    [self btstackScan];
}

- (void)stopScan
{
    [self btstackStopScan];
}

- (void)shutdown
{
    [self btstackShutdown];
}

- (void)sendCommand:(MRBTCommand)cmd
{
    [self btstackSendCommand:cmd];
}

#pragma mark - BTstack

- (void)btstackSetupManager
{
    _btstackManager = [BTstackManager sharedInstance];
    _btstackManager.delegate = self;
    [_btstackManager addListener:self];
}

- (void)btstackStart
{
    [_btstackManager activate];
}

- (void)btstackScan
{
    [_btstackManager startDiscovery];
}

- (void)btstackStopScan
{
    [_btstackManager stopDiscovery];
}

- (void)btstackShutdown
{
    [_btstackManager closeRFCOMMConnectionWithID:_btstackManager.srcChannelID];
    [_btstackManager deactivate];
}

- (void)btstackSendCommand:(MRBTCommand)cmd
{
    if (![_btstackManager isActive])
    {
        return;
    }
    
    switch (cmd)
    {
        case MRBTCommandShutdown:
            break;
        case MRBTCommandEnter:
            break;
            
        case MRBTCommandLock:
            break;
            
        case MRBTCommandMenu:
            break;
            
        case MRBTCommandNext:
            break;
            
        case MRBTCommandPrevious:
            break;
            
        case MRBTCommandNumber0:
            break;
            
        case MRBTCommandNumber1:
        {
            char *d = "RE#N1\n\r";
            [_btstackManager sendRFCOMMData:(uint8_t *)d length:strlen(d)];
        }
            break;
            
        case MRBTCommandNumber2:
            break;
            
        case MRBTCommandNumber3:
            break;
            
        case MRBTCommandNumber4:
            break;
            
        case MRBTCommandNumber5:
            break;
            
        case MRBTCommandNumber6:
            break;
            
        case MRBTCommandNumber7:
            break;
            
        case MRBTCommandNumber8:
            break;
            
        case MRBTCommandNumber9:
            break;
            
        default:
            break;
    }
}

#pragma mark BTstackManger delegate

-(void) btstackManager:(BTstackManager*) manager
  handlePacketWithType:(uint8_t) packet_type
			forChannel:(uint16_t) channel
			   andData:(uint8_t *)packet
			   withLen:(uint16_t) size
{
    BTDevice *device = (BTDevice *)_currentDevice;
    bd_addr_t event_addr;
	
	switch (packet_type)
    {
		case RFCOMM_DATA_PACKET:
            if (1)
            {
                char bf[64];
                memcpy(bf, packet, size);
                bf[size] = '\0';
                
                printf("Received RFCOMM data on channel id %u, size %u\n %s", channel, size, bf);
			}
			break;
			
		case HCI_EVENT_PACKET:
			switch (packet[0])
            {
				case HCI_EVENT_COMMAND_COMPLETE:
                    NSLog(@"HCI_EVENT_COMMAND_COMPLETE");
                    if ( COMMAND_COMPLETE_EVENT(packet, hci_write_authentication_enable) )
                    {
                        bt_send_cmd(&rfcomm_create_channel, device.address, 1);
                    }
					break;
                    
				case HCI_EVENT_PIN_CODE_REQUEST:
					bt_flip_addr(event_addr, &packet[2]);
                    bt_send_cmd(&hci_pin_code_request_reply, &event_addr, 4, "0000");
					break;
                    
                case HCI_EVENT_DISCONNECTION_COMPLETE:
					NSLog(@"bt: Basebank connection closed");
                    manager.srcChannelID = 0;
                    VT_POST_NOTIFICATION(kMRBluetoothDisconnectedNotification, [NSNull null]);
					break;
                    
				case RFCOMM_EVENT_OPEN_CHANNEL_COMPLETE:
					// data: event(8), len(8), status (8), address (48), handle(16), server channel(8), rfcomm_cid(16), max frame size(16)
					if (packet[2])
                    {
						NSLog(@"RFCOMM channel open failed, status %u", packet[2]);
					}
                    else
                    {
                        manager.srcChannelID = READ_BT_16(packet, 12);
						uint16_t mtu = READ_BT_16(packet, 14);
						NSLog(@"RFCOMM channel open succeeded. New RFCOMM Channel ID %u, max frame size %u, server C %u", manager.srcChannelID, mtu, packet[11]);
                        
                        VT_POST_NOTIFICATION(kMRBluetoothConnectedNotification, [NSNull null]);
					}
					break;
                    
                case RFCOMM_EVENT_CHANNEL_CLOSED:
                    NSLog(@"RFCOMM_EVENT_CHANNEL_CLOSED");
                    VT_POST_NOTIFICATION(kMRBluetoothDisconnectedNotification, [NSNull null]);
                    break;
                    
                case RFCOMM_EVENT_INCOMING_CONNECTION:
                    NSLog(@"RFCOMM_EVENT_INCOMING_CONNECTION");
                    break;
                    
				default:
					break;
			}
			break;
		default:
			break;
	}
}

-(void)activatedBTstackManager:(BTstackManager*)manager
{
	NSLog(@"bt: activated!");
	[manager startDiscovery];
}

-(void)btstackManager:(BTstackManager*)manager activationFailed:(BTstackError)error
{
	NSLog(@"bt: activationFailed error 0x%02x!", error);
}

-(void)deactivatedBTstackManager:(BTstackManager*)manager
{
    NSLog(@"bt: deactivated!");
    
    VT_POST_NOTIFICATION(kMRBluetoothDisconnectedNotification, [NSNull null]);
}

-(void)discoveryInquiryBTstackManager:(BTstackManager*)manager
{
	NSLog(@"bt: discoveryInquiry!");
}

-(void) discoveryStoppedBTstackManager:(BTstackManager*)manager
{
	NSLog(@"bt: discoveryStopped!");
    
    BTDevice *d = (BTDevice *)_currentDevice;
    BTstackError err = [manager createRFCOMMConnectionAtAddress:d.address withChannel:1 authenticated:NO];
    NSLog(@"bt: connect to %@ (err %d)", d.name, err);
}

-(void) btstackManager:(BTstackManager*)manager discoveryQueryRemoteName:(int)deviceIndex
{
	NSLog(@"bt: discoveryQueryRemoteName %u/%u!", deviceIndex+1, [manager numberOfDevicesFound]);
}

-(void) btstackManager:(BTstackManager*)manager deviceInfo:(BTDevice*)device
{
	NSLog(@"bt: Device Info: addr %@ name %@ COD 0x%06x", [device addressString], [device name], [device classOfDevice]);
    
    if ([device.name hasPrefix:@"APM031"])
//    if ([device.name hasPrefix:@"launch05"])
    {
        self.currentDevice = device;
        [manager stopDiscovery];
    }
}

#pragma mark - Bluetooth Manager

- (void)setupBTManager
{
    _btManager = [BluetoothManager sharedInstance];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceDiscovered:)
                                                 name:@"BluetoothDeviceDiscoveredNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bluetoothAvailabilityChanged:)
                                                 name:@"BluetoothAvailabilityChangedNotification"
                                               object:nil];
    
//    CFNotificationCenterAddObserver(CFNotificationCenterGetLocalCenter(),
//                                    NULL,
//                                    myCallBack,
//                                    NULL,
//                                    NULL,
//                                    CFNotificationSuspensionBehaviorDeliverImmediately);
}

- (void)unsetupMTManager
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)startBTManager
{
    if (!_btManager.powered)
    {
        [_btManager setPowered:YES];
    }
    
    if (!_btManager.enabled)
    {
        [_btManager setEnabled:YES];
    }
}

- (void)shutdownBTManager
{
    [_btManager setEnabled:NO];
    [_btManager setPowered:NO];
}

- (void)btManagerscan
{
    [_btManager setDeviceScanningEnabled:YES];
}

- (void)btManagerstopScan
{
    [_btManager setDeviceScanningEnabled:NO];
}

- (void)btManagerConnectTo:(BluetoothDevice *)device
{
    __block MRBlueTooth *myself = self;
    __block int i = 0;
    
    [device connect];
    
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
    dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), 1ull*NSEC_PER_SEC, 1ull*NSEC_PER_SEC);
    
    dispatch_source_set_event_handler(timer, ^{
        NSLog(@"BT connect to:>>>> %d", device.connected);
        
        i++;
        
        if (!myself.btManager.powered || i > 30)
        {
            [myself btManagerscan];
            dispatch_source_cancel(timer);
            VT_POST_NOTIFICATION(kMRBluetoothDisconnectedNotification, [NSNull null]);
        }
        
        if (device.connected)
        {
            [myself btManagerscan];
            dispatch_source_cancel(timer);
            VT_POST_NOTIFICATION(kMRBluetoothConnectedNotification, [NSNull null]);
        }
    });
    
    dispatch_source_set_cancel_handler(timer, ^{
        dispatch_release(timer);
    });
    
    dispatch_resume(timer);
}

- (void)deviceDiscovered:(NSNotification *)notify
{
    BluetoothDevice *bt = notify.object;
    NSLog(@">> BT discover device: %@ %@",bt.name, bt.address);
    
    if ([bt.name hasPrefix:@"AEC-SDK"])
    {
        self.currentDevice = bt;
        [self btManagerConnectTo:bt];
//        [_devices addObject:bt];
    }
}

- (void)bluetoothAvailabilityChanged:(NSNotification *)notify
{
    NSLog(@">> BT State: %d", [_btManager enabled]);
}

#pragma mark - CoreBluetooth

- (void)setupCoreBT
{
    _centralManager =[[CBCentralManager alloc] initWithDelegate:self
                                                          queue:dispatch_get_global_queue(0, 0)];
    
    
    
    _peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                                 queue:dispatch_get_global_queue(0, 0)];
}

#pragma mark CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSLog(@"%@", advertisementData);
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"centralManager did update state");
}

#pragma mark CBPeripheralManagerDelegate

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    NSLog(@"peripheralManager did update state");
}

@end
