//
//  MRBlueTooth.h
//  MiboRibbit
//
//  Created by launch05 on 13-11-11.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import <Foundation/Foundation.h>

//BluetoothManager
#import "BluetoothManager.h"
#import "BluetoothDevice.h"
#import "BluetoothAudioJack.h"

//CoreBluetooth
#import <CoreBluetooth/CoreBluetooth.h>

//BTstack
#import "BTDevice.h"
#import "BTstackManager.h"


static NSString *const kMRBluetoothConnectedNotification = @"MRBluetoothConnectedNotification";

static NSString *const kMRBluetoothDisconnectedNotification = @"MRBluetoothDisconnectedNotification";

typedef enum {
    MRBTCommandShutdown,
    MRBTCommandEnter,
    MRBTCommandNumber1,
    MRBTCommandNumber2,
    MRBTCommandNumber3,
    MRBTCommandNumber4,
    MRBTCommandNumber5,
    MRBTCommandNumber6,
    MRBTCommandNumber7,
    MRBTCommandNumber8,
    MRBTCommandNumber9,
    MRBTCommandNumber0,
    MRBTCommandVolumeUp,
    MRBTCommandVolumeDown,
    MRBTCommandNext,
    MRBTCommandPrevious,
    MRBTCommandMenu,
    MRBTCommandLock,
    MRBTCommandPlayPause,
    MRBTCommandRepeat,
    MRBTCommandTimeShutDown,
    MRBTCommandStory,
    MRBTCommandSong
} MRBTCommand;

@interface MRBlueTooth : NSObject <CBCentralManagerDelegate, CBPeripheralManagerDelegate, BTstackManagerDelegate, BTstackManagerListener>

@property (nonatomic, readonly) BTstackManager *btstackManager;
@property (nonatomic, assign) BluetoothManager *btManager;
@property (nonatomic, assign) CBCentralManager *centralManager;
@property (nonatomic, assign) CBPeripheralManager *peripheralManager;

+ (MRBlueTooth *)sharedInstance;

- (void)start;

- (void)shutdown;

- (void)scan;

- (void)stopScan;

- (void)sendCommand:(MRBTCommand)cmd;

@end
