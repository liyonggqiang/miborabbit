//
//  MRMaskButton.m
//  MiboRibbit
//
//  Created by Li Steven on 13-11-10.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRMaskButton.h"

@implementation MRMaskButton

- (void)dealloc
{
    free(_imageDataBuffer);
    
    [super dealloc];
}

//- (id):(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (void)awakeFromNib
{
//    self.userInteractionEnabled = YES;
    
    UIImage *image = [self imageForState:UIControlStateNormal];
    
    UIGraphicsBeginImageContextWithOptions(image.size, YES, 1.0);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    _w = CGBitmapContextGetWidth(ctx);
    _h = CGBitmapContextGetHeight(ctx);
    
    UInt32 *data = CGBitmapContextGetData(ctx);
    _imageDataBuffer = (UInt32 *)malloc(_w*_h*sizeof(UInt32));
    memcpy(_imageDataBuffer, (const void *)data, _w*_h*sizeof(UInt32));
    
    UIGraphicsEndImageContext();
    
    [self setImage:image forState:UIControlStateHighlighted];
    [self setImage:nil forState:UIControlStateNormal];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL b = [super pointInside:point withEvent:event];
    
    if (!b)
    {
        return NO;
    }
    
    if (0.0 > point.x || 0.0 > point.y)
    {
        return NO;
    }
    
    if (point.x > _w || point.y > _h)
    {
        return NO;
    }
    
    uint8_t alpha = _imageDataBuffer[(int)point.x+_w*(int)point.y] >> 24;
    
    return alpha != 0;
}

@end
