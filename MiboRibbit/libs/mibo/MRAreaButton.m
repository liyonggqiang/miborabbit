//
//  MRAreaButton.m
//  MiboRibbit
//
//  Created by Li Steven on 13-12-1.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import "MRAreaButton.h"

@implementation MRAreaButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL b = [super pointInside:point withEvent:event];
    
    if (!b)
    {
        return NO;
    }
    
    if (point.y < self.bounds.size.height * 0.5)
    {
        _clickedArea = MRAreaButtonAreaTop;
    }
    else
    {
        _clickedArea = MRAreaButtonAreaBottom;
    }
    
    return YES;
}

@end
