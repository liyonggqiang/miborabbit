//
//  MRMaskButton.h
//  MiboRibbit
//
//  Created by Li Steven on 13-11-10.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRMaskButton : UIButton
{
    UInt32 *_imageDataBuffer;
    size_t _w;
    size_t _h;
}

@end
