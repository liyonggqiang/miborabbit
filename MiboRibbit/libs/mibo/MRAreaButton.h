//
//  MRAreaButton.h
//  MiboRibbit
//
//  Created by Li Steven on 13-12-1.
//  Copyright (c) 2013年 CoderFly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    MRAreaButtonAreaTop,
    MRAreaButtonAreaBottom
} MRAreaButtonArea;

@interface MRAreaButton : UIButton

@property (nonatomic, readonly) MRAreaButtonArea clickedArea;

@end
